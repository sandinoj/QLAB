For a video of the algorithm please click on the image below.

[![Autonomous Quadrotor Landing using Deep Reinforcement Learning](./img.png)](https://youtu.be/Mnaav9QQ49k "Sim-to-Real Quadrotor Landing via Hierarchical Deep Q-Networks and Domain Randomization")
